//
//  ChatInputContainerView.swift
//  gameofchats
//
//  Created by Brian Voong on 8/10/16.
//  Copyright © 2016 letsbuildthatapp. All rights reserved.
//

import UIKit

class ChatInputContainerView: UIView, UITextFieldDelegate {
    
    public var toolbar: UIToolbar!
    
    weak var chatLogController: ChatLogController? {
        didSet {
            sendButton.addTarget(chatLogController, action: #selector(ChatLogController.handleSend), for: .touchUpInside)
            uploadImageView.addGestureRecognizer(UITapGestureRecognizer(target: chatLogController, action: #selector(ChatLogController.handleUploadTap)))
        }
    }
    
    lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Type a message..."
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()
    
    let uploadImageView: UIImageView = {
        let uploadImageView = UIImageView()
        uploadImageView.isUserInteractionEnabled = true
        uploadImageView.image = UIImage(named: "upload_image_icon")
        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
        return uploadImageView
    }()
    
    let sendButton = UIButton(type: .system)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        toolbar = UIToolbar(frame: frame)
        toolbar.backgroundColor = .white
        toolbar.barTintColor = .white
        addSubview(toolbar)
        autoresizingMask = .flexibleHeight
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        toolbar.leadingAnchor.constraint(
                    equalTo: self.leadingAnchor,
                    constant: 0
                ).isActive = true
                toolbar.trailingAnchor.constraint(
                    equalTo: self.trailingAnchor,
                    constant: 0
                ).isActive = true
                toolbar.topAnchor.constraint(
                    equalTo: self.topAnchor,
                    constant: 0
                ).isActive = true
                // This is the important part:
                if #available(iOS 11.0, *) {
                    toolbar.bottomAnchor.constraint(
                        equalTo: self.safeAreaLayoutGuide.bottomAnchor,
                        constant: 0
                    ).isActive = true
                } else {
                    toolbar.bottomAnchor.constraint(
                        equalTo: self.layoutMarginsGuide.bottomAnchor,
                        constant: 0
                    ).isActive = true
                }
        
        sendButton.setTitle("Send", for: .normal)
        sendButton.backgroundColor = .orange
        sendButton.layer.cornerRadius = 5
        sendButton.setTitleColor(.white, for: .normal)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        
        
        //what is handleSend?
        
        addSubview(sendButton)
        //x,y,w,h
        sendButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        addSubview(self.inputTextField)
        //x,y,w,h
        self.inputTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        self.inputTextField.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        self.inputTextField.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        let separatorLineView = UIView()
        separatorLineView.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separatorLineView)
        //x,y,w,h
        separatorLineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorLineView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        separatorLineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        separatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        chatLogController?.handleSend()
        return true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
            return CGSize.zero
        }
    
}
